package com.carsome.bookingservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.carsome.bookingservice.dto.BookingInspectionDto;
import com.carsome.bookingservice.entity.BookingInspection;
import com.carsome.bookingservice.entity.BookingSlot;
import com.carsome.bookingservice.repository.BookingInspectionRepository;
import com.carsome.bookingservice.repository.BookingScheduleRepository;
import com.carsome.bookingservice.repository.BookingSlotRepository;
import com.carsome.bookingservice.service.exception.ResourceNotFoundException;
import com.carsome.bookingservice.service.exception.ValidationException;
import com.carsome.bookingservice.service.exception.ValidationExceptionCode;
import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BookingServiceTest {
  @InjectMocks private BookingInspectionService service;

  @Mock private BookingInspectionRepository bookingInspectionRepository;
  @Mock private BookingSlotRepository bookingSlotRepository;
  @Mock private BookingScheduleRepository bookingScheduleRepository;

  private ZonedDateTime generateWeekdaysDate(Long addDays) {
    ZonedDateTime dateTime = ZonedDateTime.now().plusDays(addDays);

    // check when its saturday or sunday
    if (dateTime.getDayOfWeek() == DayOfWeek.SATURDAY) {
      dateTime = dateTime.plusDays(2);
    } else if (dateTime.getDayOfWeek() == DayOfWeek.SUNDAY) {
      dateTime = dateTime.plusDays(1);
    }

    return dateTime;
  }

  private ZonedDateTime generateWeekendDate(Long addDays, DayOfWeek dayOfWeek) {
    ZonedDateTime dateTime = ZonedDateTime.now().plusDays(addDays);

    // check when its saturday or sunday
    while (dateTime.getDayOfWeek() != dayOfWeek) {
      dateTime = dateTime.plusDays(1);
    }

    return dateTime;
  }

  private String generateWeekdaysInspectionDate() {
    return generateWeekdaysInspectionDate(7l);
  }

  private String generateWeekdaysInspectionDate(Long addDays) {
    ZonedDateTime dateTime = generateWeekdaysDate(addDays);

    // set time to 9.00
    dateTime = dateTime.withHour(9).withMinute(0).withSecond(0).withNano(0);

    return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
  }

  private String generateSaturdayInspectionDate() {
    ZonedDateTime dateTime = generateWeekendDate(7l, DayOfWeek.SATURDAY);

    // set time to 9.00
    dateTime = dateTime.withHour(9).withMinute(0).withSecond(0).withNano(0);

    return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
  }

  private String generateSundayInspectionDate() {
    ZonedDateTime dateTime = generateWeekendDate(7l, DayOfWeek.SUNDAY);

    return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
  }

  private BookingInspectionDto generateBookingInspectionDto(String date) {
    return BookingInspectionDto.builder()
        .inspectionDate(date)
        .customerName("customer name")
        .customerEmail("me@mail.com")
        .customerPhoneNo("088888888")
        .build();
  }

  @Test
  public void bookingInspectionEmptyInspectionDate_failed() {
    BookingInspectionDto request = new BookingInspectionDto();

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(
        ValidationExceptionCode.INVALID_PARAM_REQUEST.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionInvalidInspectionDateFormat_failed() {
    BookingInspectionDto request =
        BookingInspectionDto.builder().inspectionDate("1234567890").build();

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(
        ValidationExceptionCode.INVALID_PARAM_REQUEST.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionEmptyCustomerName_failed() {
    BookingInspectionDto request =
        BookingInspectionDto.builder().inspectionDate("2020-01-01 00:00:00").build();

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(
        ValidationExceptionCode.INVALID_PARAM_REQUEST.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionEmptyCustomerEmail_failed() {
    BookingInspectionDto request =
        BookingInspectionDto.builder()
            .inspectionDate("2020-01-01 00:00:00")
            .customerName("customer name")
            .build();

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(
        ValidationExceptionCode.INVALID_PARAM_REQUEST.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionInvalidCustomerEmailFormat_failed() {
    BookingInspectionDto request =
        BookingInspectionDto.builder()
            .inspectionDate("2020-01-01 00:00:00")
            .customerName("customer name")
            .customerEmail("customer email")
            .build();

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.INVALID_PARAM_FORMAT.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionEmptyCustomerPhone_failed() {
    BookingInspectionDto request =
        BookingInspectionDto.builder()
            .inspectionDate("2020-01-01 00:00:00")
            .customerName("customer name")
            .customerEmail("me@mail.com")
            .build();

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(
        ValidationExceptionCode.INVALID_PARAM_REQUEST.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionPastTime_failed() {
    BookingInspectionDto request = generateBookingInspectionDto("2000-01-01 00:00:00");

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.PAST_TIME.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionSunday_failed() {
    BookingInspectionDto request = generateBookingInspectionDto(generateSundayInspectionDate());

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.INVALID_DAY.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionBefore8Clock_failed() {
    String dateTime = generateWeekdaysInspectionDate().replace("09:00:00", "08:59:59");
    BookingInspectionDto request = generateBookingInspectionDto(dateTime);

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.OUT_OF_WORKING_HOUR.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionAfter18Clock_failed() {
    String dateTime = generateWeekdaysInspectionDate().replace("09:00:00", "19:00:00");
    BookingInspectionDto request = generateBookingInspectionDto(dateTime);

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.OUT_OF_WORKING_HOUR.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionInvalidBookingSlotTime_failed() {
    String dateTime = generateWeekdaysInspectionDate().replace("09:00:00", "09:01:00");
    BookingInspectionDto request = generateBookingInspectionDto(dateTime);

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.INVALID_SLOT_TIME.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionAfter3Weeks_failed() {
    String dateTime = generateWeekdaysInspectionDate(28l);
    BookingInspectionDto request = generateBookingInspectionDto(dateTime);

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.INVALID_PERIOD.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionLimitExceeded_failed() {
    BookingInspectionDto request = generateBookingInspectionDto(generateWeekdaysInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(BookingInspection.builder().createdDate(ZonedDateTime.now()).build());

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.TO_MUCH_BOOKING.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionBookingSlotExceededWeekdays_failed() {
    BookingInspectionDto request = generateBookingInspectionDto(generateWeekdaysInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class)))
        .thenReturn(
            BookingSlot.builder().inspectionDate(generateWeekdaysDate(7l)).booked(2).build());

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.NO_SLOT_AVAILABLE.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionBookingSlotExceededSaturday_failed() {
    BookingInspectionDto request = generateBookingInspectionDto(generateSaturdayInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class)))
        .thenReturn(
            BookingSlot.builder().inspectionDate(generateWeekdaysDate(7l)).booked(3).build());

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.NO_SLOT_AVAILABLE.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionBookingSlotWeekdays_success() {
    BookingInspectionDto request = generateBookingInspectionDto(generateWeekdaysInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class))).thenReturn(null);
    when(bookingInspectionRepository.save(any(BookingInspection.class)))
        .thenReturn(BookingInspection.builder().id(1l).inspectionDate(ZonedDateTime.now()).build());
    when(bookingSlotRepository.save(any(BookingSlot.class)))
        .thenReturn(BookingSlot.builder().id(1l).build());
    when(bookingInspectionRepository.countByInspectionDate(any(ZonedDateTime.class))).thenReturn(1);
    doNothing().when(bookingScheduleRepository).deleteSchedule();

    BookingInspectionDto response = service.bookInspection(request);

    verify(bookingInspectionRepository, times(1)).save(any(BookingInspection.class));
    verify(bookingSlotRepository, times(1)).save(argThat(slot -> slot.getBooked() == 1l));
    assertEquals(1l, response.getId());
  }

  @Test
  public void bookingInspectionBooking2ndSlotWeekdays_success() {
    BookingInspectionDto request = generateBookingInspectionDto(generateWeekdaysInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class)))
        .thenReturn(
            BookingSlot.builder().inspectionDate(generateWeekdaysDate(7l)).booked(1).build());
    when(bookingInspectionRepository.save(any(BookingInspection.class)))
        .thenReturn(BookingInspection.builder().id(1l).inspectionDate(ZonedDateTime.now()).build());
    when(bookingSlotRepository.save(any(BookingSlot.class)))
        .thenReturn(BookingSlot.builder().id(1l).build());
    when(bookingInspectionRepository.countByInspectionDate(any(ZonedDateTime.class))).thenReturn(1);
    doNothing().when(bookingScheduleRepository).deleteSchedule();

    BookingInspectionDto response = service.bookInspection(request);

    verify(bookingInspectionRepository, times(1)).save(any(BookingInspection.class));
    verify(bookingSlotRepository, times(1)).save(argThat(slot -> slot.getBooked() == 2l));
    assertEquals(1l, response.getId());
  }

  @Test
  public void bookingInspectionBooking3rdSlotWeekdays_failed() {
    BookingInspectionDto request = generateBookingInspectionDto(generateWeekdaysInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class)))
        .thenReturn(
            BookingSlot.builder().inspectionDate(generateWeekdaysDate(7l)).booked(2).build());

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.NO_SLOT_AVAILABLE.toString(), exception.getErrorCode());
  }

  @Test
  public void bookingInspectionBookingSlotSaturday_success() {
    BookingInspectionDto request = generateBookingInspectionDto(generateSaturdayInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class))).thenReturn(null);
    when(bookingInspectionRepository.save(any(BookingInspection.class)))
        .thenReturn(BookingInspection.builder().id(1l).inspectionDate(ZonedDateTime.now()).build());
    when(bookingSlotRepository.save(any(BookingSlot.class)))
        .thenReturn(BookingSlot.builder().id(1l).build());
    when(bookingInspectionRepository.countByInspectionDate(any(ZonedDateTime.class))).thenReturn(1);
    doNothing().when(bookingScheduleRepository).deleteSchedule();

    BookingInspectionDto response = service.bookInspection(request);

    verify(bookingInspectionRepository, times(1)).save(any(BookingInspection.class));
    verify(bookingSlotRepository, times(1)).save(argThat(slot -> slot.getBooked() == 1l));
    assertEquals(1l, response.getId());
  }

  @Test
  public void bookingInspectionBooking3ndSlotSaturday_success() {
    BookingInspectionDto request = generateBookingInspectionDto(generateSaturdayInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class)))
        .thenReturn(
            BookingSlot.builder()
                .inspectionDate(generateWeekendDate(7l, DayOfWeek.SATURDAY))
                .booked(2)
                .build());
    when(bookingInspectionRepository.save(any(BookingInspection.class)))
        .thenReturn(BookingInspection.builder().id(1l).inspectionDate(ZonedDateTime.now()).build());
    when(bookingSlotRepository.save(any(BookingSlot.class)))
        .thenReturn(BookingSlot.builder().id(1l).build());
    doNothing().when(bookingScheduleRepository).deleteSchedule();

    BookingInspectionDto response = service.bookInspection(request);

    verify(bookingInspectionRepository, times(1)).save(any(BookingInspection.class));
    verify(bookingSlotRepository, times(1)).save(argThat(slot -> slot.getBooked() == 3l));
    assertEquals(1l, response.getId());
  }

  @Test
  public void bookingInspectionBooking4thSlotSaturday_failed() {
    BookingInspectionDto request = generateBookingInspectionDto(generateSaturdayInspectionDate());

    when(bookingInspectionRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            eq(request.getCustomerEmail())))
        .thenReturn(null);
    when(bookingSlotRepository.findOneByInspectionDate(any(ZonedDateTime.class)))
        .thenReturn(
            BookingSlot.builder()
                .inspectionDate(generateWeekendDate(7l, DayOfWeek.SATURDAY))
                .booked(3)
                .build());

    ValidationException exception =
        assertThrows(ValidationException.class, () -> service.bookInspection(request));

    assertEquals(ValidationExceptionCode.NO_SLOT_AVAILABLE.toString(), exception.getErrorCode());
  }

  @Test
  public void getBookingByIdNotFound_failed() {
    when(bookingInspectionRepository.findById(anyLong())).thenReturn(Optional.empty());

    assertThrows(ResourceNotFoundException.class, () -> service.getById(0l));
  }
}
