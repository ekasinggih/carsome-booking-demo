package com.carsome.bookingservice.service.exception;

public enum ValidationExceptionCode {
  INVALID_PARAM_REQUEST("000"),
  INVALID_PARAM_FORMAT("001"),
  PAST_TIME("002"),
  INVALID_DAY("003"),
  OUT_OF_WORKING_HOUR("004"),
  INVALID_SLOT_TIME("005"),
  INVALID_PERIOD("006"),
  TO_MUCH_BOOKING("007"),
  NO_SLOT_AVAILABLE("008");

  private String value;

  ValidationExceptionCode(String validationExceptionCode) {
    this.value = validationExceptionCode;
  }

  public String value() {
    return this.value;
  }

  @Override
  public String toString() {
    return this.value;
  }
}
