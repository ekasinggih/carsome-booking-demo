package com.carsome.bookingservice.service.exception;

import lombok.Getter;
import lombok.Setter;

public class ValidationException extends RuntimeException {

  private static final long serialVersionUID = 5270233049725723685L;

  @Getter @Setter private String errorCode;

  ValidationException(String errorCode) {
    super();
    this.errorCode = errorCode;
  }

  ValidationException(ValidationExceptionCode code, String message) {
    super(message);
    this.errorCode = code.value();
  }

  ValidationException(ValidationExceptionCode code, String message, Throwable cause) {
    super(message, cause);
    this.errorCode = code.value();
  }

  public static final ValidationException invalidParamRequest(Throwable throwable) {
    return new ValidationException(
        ValidationExceptionCode.INVALID_PARAM_REQUEST, "invalid parameter request", throwable);
  }

  public static final ValidationException emptyParamRequest(String field) {
    return new ValidationException(
        ValidationExceptionCode.INVALID_PARAM_REQUEST,
        String.format("empty param request %s", field));
  }

  public static final ValidationException invalidParamRequestFormat(String field) {
    return new ValidationException(
        ValidationExceptionCode.INVALID_PARAM_FORMAT,
        String.format("invalid param request format %s", field));
  }

  public static final ValidationException pastTimeBooking() {
    return new ValidationException(
        ValidationExceptionCode.PAST_TIME, "can't booking inspection for past time");
  }

  public static final ValidationException sundayBooking() {
    return new ValidationException(
        ValidationExceptionCode.INVALID_DAY, "can't booking inspection for sunday");
  }

  public static final ValidationException outOfWorkingHour() {
    return new ValidationException(
        ValidationExceptionCode.OUT_OF_WORKING_HOUR, "inspection time outside working hours");
  }

  public static final ValidationException invalidSlotTime() {
    return new ValidationException(
        ValidationExceptionCode.INVALID_SLOT_TIME, "invalid inspection slot time");
  }

  public static final ValidationException outOfPeriod(Integer period) {
    return new ValidationException(
        ValidationExceptionCode.INVALID_PERIOD,
        String.format("out of period booking, please make booking within %d days", period));
  }

  public static final ValidationException toMuchBooking(Integer tryAgainIn) {
    return new ValidationException(
        ValidationExceptionCode.TO_MUCH_BOOKING,
        String.format("to much booking, please try again in %d minutes", tryAgainIn));
  }

  public static final ValidationException noSlotAvailable() {
    return new ValidationException(
        ValidationExceptionCode.NO_SLOT_AVAILABLE, "no slot available, please select another time");
  }
}
