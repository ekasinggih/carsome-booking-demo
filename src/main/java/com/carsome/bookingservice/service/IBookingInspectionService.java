package com.carsome.bookingservice.service;

import com.carsome.bookingservice.dto.BookingInspectionDto;
import com.carsome.bookingservice.dto.BookingScheduleDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;

public interface IBookingInspectionService {
  BookingInspectionDto bookInspection(BookingInspectionDto request);

  List<BookingScheduleDto> getAvailableSchedule() throws JsonProcessingException;

  BookingInspectionDto getById(Long id);
}
