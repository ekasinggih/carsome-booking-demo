package com.carsome.bookingservice.service;

import com.carsome.bookingservice.converter.BookingInspectionConverter;
import com.carsome.bookingservice.converter.BookingScheduleConverter;
import com.carsome.bookingservice.converter.DateTimeConverter;
import com.carsome.bookingservice.dto.BookingInspectionDto;
import com.carsome.bookingservice.dto.BookingScheduleDto;
import com.carsome.bookingservice.entity.BookingInspection;
import com.carsome.bookingservice.entity.BookingSchedule;
import com.carsome.bookingservice.entity.BookingSlot;
import com.carsome.bookingservice.repository.BookingInspectionRepository;
import com.carsome.bookingservice.repository.BookingScheduleRepository;
import com.carsome.bookingservice.repository.BookingSlotRepository;
import com.carsome.bookingservice.service.exception.ResourceNotFoundException;
import com.carsome.bookingservice.service.exception.ValidationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.DayOfWeek;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Service
public class BookingInspectionService implements IBookingInspectionService {

  @Autowired private BookingInspectionRepository bookingRepository;
  @Autowired private BookingSlotRepository bookingSlotRepository;
  @Autowired private BookingScheduleRepository bookingScheduleRepository;

  private static final Integer BOOKING_PERIOD_DAYS = 21;
  private static final Integer BOOKING_THROTTLING_MINUTES = 180;
  private static final Integer INSPECTION_TIME_HOUR_START = 9;
  private static final Integer INSPECTION_TIME_HOUR_END = 18;
  private static final Integer INSPECTION_TIME_EVERY_MINUTES = 30;

  @Override
  @Transactional
  public BookingInspectionDto bookInspection(BookingInspectionDto request) {
    // validate request
    this.validateBookInspectionRequest(request);

    BookingInspection bookingInspection = BookingInspectionConverter.toEntity(request);

    // business validation
    BookingSlot bookingSlot = this.validateBookInspection(bookingInspection);

    // save booking
    BookingInspection booking = this.createBookingInspection(bookingInspection, bookingSlot);

    // make sure booking slot not exceeded due concurrency issue
    this.makeSureBookingSlotNotExceeded(bookingInspection.getInspectionDate());

    // invalidate cache booking schedule
    bookingScheduleRepository.deleteSchedule();

    return BookingInspectionConverter.toDto(booking);
  }

  private void validateBookInspectionRequest(BookingInspectionDto request) {
    try {
      this.validateNotNullOrEmpty(request.getInspectionDate(), "InspectionDate");
      DateTimeConverter.toZonedDateTime(request.getInspectionDate());
      this.validateNotNullOrEmpty(request.getCustomerName(), "CustomerName");
      this.validateNotNullOrEmpty(request.getCustomerEmail(), "CustomerEmail");
      this.validateEmailAddress(request.getCustomerEmail(), "CustomerEmail");
      this.validateNotNullOrEmpty(request.getCustomerPhoneNo(), "CustomerPhone");
    } catch (ValidationException ve) {
      throw ve;
    } catch (Exception e) {
      throw ValidationException.invalidParamRequest(e);
    }
  }

  private void validateNotNullOrEmpty(String input, String field) {
    if (StringUtils.isEmpty(input)) {
      throw ValidationException.emptyParamRequest(field);
    }
  }

  private void validateEmailAddress(String emailAddress, String field) {
    if (!EmailValidator.getInstance(true).isValid(emailAddress)) {
      throw ValidationException.invalidParamRequestFormat(field);
    }
  }

  private BookingSlot validateBookInspection(BookingInspection bookingInspection) {
    // validate inspection date
    this.validateBookingInspectionDateTime(bookingInspection);

    // validate booking limit
    this.validateBookingLimit(bookingInspection);

    // validate available slot
    return this.validateAvailableSlot(bookingInspection);
  }

  private void validateBookingInspectionDateTime(BookingInspection bookingInspection) {
    // validate past date
    if (bookingInspection.getInspectionDate().isBefore(ZonedDateTime.now())) {
      throw ValidationException.pastTimeBooking();
    }

    // validate inspection date not on sunday
    if (bookingInspection.getInspectionDate().getDayOfWeek() == DayOfWeek.SUNDAY) {
      throw ValidationException.sundayBooking();
    }

    // validate inspection time in working hours
    Integer inspectionTimeHour = bookingInspection.getInspectionDate().toLocalTime().getHour();
    if (inspectionTimeHour < INSPECTION_TIME_HOUR_START
        || inspectionTimeHour > INSPECTION_TIME_HOUR_END) {
      throw ValidationException.outOfWorkingHour();
    }

    // validate inspection time slots
    Integer inspectionTimeMinutes = bookingInspection.getInspectionDate().toLocalTime().getMinute();
    if (inspectionTimeMinutes % INSPECTION_TIME_EVERY_MINUTES != 0) {
      throw ValidationException.invalidSlotTime();
    }

    // validate inspection date within 3 weeks
    Period inspectionPeriod =
        Period.between(
            ZonedDateTime.now().toLocalDate(), bookingInspection.getInspectionDate().toLocalDate());
    Integer periodInDays = inspectionPeriod.getDays();

    if (periodInDays < 0 || periodInDays > BOOKING_PERIOD_DAYS) {
      throw ValidationException.outOfPeriod(BOOKING_PERIOD_DAYS);
    }
  }

  private void validateBookingLimit(BookingInspection bookingInspection) {
    // validate last booking after 3 hours
    BookingInspection lastBookingInspection =
        bookingRepository.findFirstByCustomerEmailOrderByCreatedDateDesc(
            bookingInspection.getCustomerEmail());

    if (Objects.nonNull(lastBookingInspection)) {
      Long lastBookingIntervalInMinutes =
          ChronoUnit.MINUTES.between(
              lastBookingInspection.getCreatedDate().toLocalDateTime(),
              ZonedDateTime.now().toLocalDateTime());
      if (lastBookingIntervalInMinutes < BOOKING_THROTTLING_MINUTES) {
        throw ValidationException.toMuchBooking(
            BOOKING_THROTTLING_MINUTES - lastBookingIntervalInMinutes.intValue());
      }
    }
  }

  Integer getMaxSlot(ZonedDateTime zonedDateTime) {
    return zonedDateTime.toLocalDate().getDayOfWeek() == DayOfWeek.SATURDAY ? 3 : 2;
  }

  private BookingSlot validateAvailableSlot(BookingInspection bookingInspection) {
    BookingSlot bookingSlot =
        bookingSlotRepository.findOneByInspectionDate(bookingInspection.getInspectionDate());

    if (Objects.nonNull(bookingSlot)) {
      Integer maxSlot = getMaxSlot(bookingSlot.getInspectionDate());
      if (bookingSlot.getBooked() >= maxSlot) {
        throw ValidationException.noSlotAvailable();
      }
    }

    return bookingSlot;
  }

  private BookingInspection createBookingInspection(
      BookingInspection bookingInspection, BookingSlot bookingSlot) {
    // prepare booking_inspection
    bookingInspection.setCreatedDate(ZonedDateTime.now());

    // prepare booking_slots
    if (Objects.isNull(bookingSlot)) {
      bookingSlot =
          BookingSlot.builder()
              .inspectionDate(bookingInspection.getInspectionDate())
              .booked(0)
              .createdDate(ZonedDateTime.now())
              .build();
    }
    bookingSlot.setBooked(bookingSlot.getBooked() + 1);

    // insert booking_inspection
    bookingInspection = bookingRepository.save(bookingInspection);

    // save booking_slots
    bookingSlotRepository.save(bookingSlot);

    return bookingInspection;
  }

  private void makeSureBookingSlotNotExceeded(ZonedDateTime dateTime) {
    Integer bookedSlot = bookingRepository.countByInspectionDate(dateTime);
    if (bookedSlot.compareTo(getMaxSlot(dateTime)) > 0) {
      throw ValidationException.noSlotAvailable();
    }
  }

  @Override
  public List<BookingScheduleDto> getAvailableSchedule() throws JsonProcessingException {
    List<BookingSchedule> schedules = bookingScheduleRepository.getSchedule();

    if (CollectionUtils.isEmpty(schedules)) {
      // populate schedule
      for (int i = 0; i < BOOKING_PERIOD_DAYS; i++) {
        // loop per date
        ZonedDateTime zonedDateTime = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).plusDays(i);

        //skip when sunday
        if(zonedDateTime.getDayOfWeek() == DayOfWeek.SUNDAY) continue;

        Date date = Date.from(zonedDateTime.toInstant());
        Integer maxSlot = getMaxSlot(zonedDateTime);
        BookingSchedule bookingSchedule = new BookingSchedule();
        bookingSchedule.setDate(date);
        // get booked slot
        List<BookingSlot> bookingSlots =
            bookingSlotRepository.findByInspectionDateBetween(
                zonedDateTime, zonedDateTime.plusDays(1));
        Map<Integer, Integer> bookingSlotMap = this.mapBookingSlot(bookingSlots);
        ZonedDateTime currentBookingTime =
            ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).withHour(INSPECTION_TIME_HOUR_START);
        List<String> timeAvailable = new ArrayList<>();
        while (currentBookingTime.getHour() <= INSPECTION_TIME_HOUR_END) {
          // loop for every slot time
          Integer minutes = currentBookingTime.get(ChronoField.MINUTE_OF_DAY);
          if (!bookingSlotMap.containsKey(minutes)
              || (bookingSlotMap.containsKey(minutes) && maxSlot > bookingSlotMap.get(minutes))) {
            timeAvailable.add(minutesToTime(minutes));
          }
          currentBookingTime = currentBookingTime.plusMinutes(30);
        }

        bookingSchedule.setAvailableTime(timeAvailable);
        schedules.add(bookingSchedule);
      }

      // set back to redis
      bookingScheduleRepository.setScheduler(new Date(), schedules);
    }

    return BookingScheduleConverter.listToDtoList(schedules);
  }

  private Map<Integer, Integer> mapBookingSlot(List<BookingSlot> bookingSlots) {
    Map<Integer, Integer> bookingSlotMap = new HashMap<>();
    for (BookingSlot bookingSlot : bookingSlots) {
      bookingSlotMap.put(
          bookingSlot.getInspectionDate().get(ChronoField.MINUTE_OF_DAY), bookingSlot.getBooked());
    }

    return bookingSlotMap;
  }

  private String minutesToTime(Integer minutes) {
    Integer h = minutes / 60;
    Integer m = minutes % 60;

    return (h >= 10 ? h.toString() : '0' + h.toString()) + ":" + (m == 30 ? "30" : "00");
  }

  @Override
  public BookingInspectionDto getById(Long id) {
    Optional<BookingInspection> bookingOpt = bookingRepository.findById(id);

    if (!bookingOpt.isPresent()) {
      throw new ResourceNotFoundException();
    }

    return BookingInspectionConverter.toDto(bookingOpt.get());
  }
}
