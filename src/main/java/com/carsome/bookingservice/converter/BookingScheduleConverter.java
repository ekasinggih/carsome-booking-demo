package com.carsome.bookingservice.converter;

import com.carsome.bookingservice.dto.BookingScheduleDto;
import com.carsome.bookingservice.entity.BookingSchedule;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class BookingScheduleConverter {
  private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

  public static List<BookingScheduleDto> listToDtoList(List<BookingSchedule> bookingScheduleList) {
    return bookingScheduleList.stream().map(b -> toDto(b)).collect(Collectors.toList());
  }

  public static BookingScheduleDto toDto(BookingSchedule bookingSchedule) {
    return BookingScheduleDto
        .builder()
        .availableDate(format.format(bookingSchedule.getDate()))
        .availableTime(bookingSchedule.getAvailableTime())
        .build();
  }
}
