package com.carsome.bookingservice.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeConverter {
  private static final DateTimeFormatter DEFAULT_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private static final ZoneId DEFAULT_ZONE = ZoneId.of("Asia/Jakarta");

  private DateTimeConverter() {}

  public static ZonedDateTime toZonedDateTime(String input) {
    return toZonedDateTime(input, DEFAULT_FORMATTER);
  }

  public static ZonedDateTime toZonedDateTime(String input, DateTimeFormatter formatter) {
    LocalDateTime localDateTime = LocalDateTime.parse(input, formatter);
    return localDateTime.atZone(DEFAULT_ZONE);
  }

  public static String toLocalDateTimeString(ZonedDateTime zonedDateTime) {
    return zonedDateTime.toLocalDateTime().format(DEFAULT_FORMATTER);
  }
}
