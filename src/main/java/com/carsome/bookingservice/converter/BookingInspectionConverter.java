package com.carsome.bookingservice.converter;

import com.carsome.bookingservice.dto.BookingInspectionDto;
import com.carsome.bookingservice.entity.BookingInspection;
import org.springframework.beans.BeanUtils;

public class BookingInspectionConverter {

  private static final String IGNORED_PROPERTIES = "id";

  private BookingInspectionConverter() {}

  public static BookingInspection toEntity(BookingInspectionDto dto) {
    BookingInspection entity = new BookingInspection();
    BeanUtils.copyProperties(dto, entity, IGNORED_PROPERTIES);
    entity.setInspectionDate(DateTimeConverter.toZonedDateTime(dto.getInspectionDate()));
    return entity;
  }

  public static BookingInspectionDto toDto(BookingInspection entity) {
    BookingInspectionDto dto = new BookingInspectionDto();
    dto.setInspectionDate(DateTimeConverter.toLocalDateTimeString(entity.getInspectionDate()));
    BeanUtils.copyProperties(entity, dto);
    return dto;
  }
}
