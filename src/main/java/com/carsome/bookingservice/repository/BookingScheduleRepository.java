package com.carsome.bookingservice.repository;

import com.carsome.bookingservice.entity.BookingSchedule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BookingScheduleRepository implements IBookingScheduleRepository {
  private static final String DATE_FORMAT = "dd-MM-yyyy";

  @Autowired private RedisTemplate<String, String> redisTemplate;
  @Autowired private ObjectMapper objectMapper;

  private String generateKey(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
    String keyValue = formatter.format(date);
    return "schedule:" + keyValue;
  }

  @Override
  public List<BookingSchedule> getSchedule() throws JsonProcessingException {
    return this.getSchedule(new Date());
  }

  @Override
  public List<BookingSchedule> getSchedule(Date date) throws JsonProcessingException {
    String key = this.generateKey(date);

    if (BooleanUtils.isTrue(redisTemplate.hasKey(key))) {
      String json = redisTemplate.opsForValue().get(key);
      return objectMapper.readValue(json, new TypeReference<List<BookingSchedule>>() {});
    }

    return new ArrayList<>();
  }

  @Override
  public void setScheduler(Date date, List<BookingSchedule> bookingSchedules)
      throws JsonProcessingException {
    String key = this.generateKey(date);
    String json = objectMapper.writeValueAsString(bookingSchedules);
    redisTemplate.opsForValue().set(key, json, Duration.ofHours(24));
  }

  @Override
  public void deleteSchedule() {
    this.deleteSchedule(new Date());
  }

  @Override
  public void deleteSchedule(Date date) {
    String key = this.generateKey(date);
    redisTemplate.delete(key);
  }
}
