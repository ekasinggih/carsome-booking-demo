package com.carsome.bookingservice.repository;

import com.carsome.bookingservice.entity.BookingSchedule;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Date;
import java.util.List;

public interface IBookingScheduleRepository {
  List<BookingSchedule> getSchedule() throws JsonProcessingException;
  List<BookingSchedule> getSchedule(Date date) throws JsonProcessingException;
  void setScheduler(Date date, List<BookingSchedule> schedules) throws JsonProcessingException;
  void deleteSchedule();
  void deleteSchedule(Date date);
}
