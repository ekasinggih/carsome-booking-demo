package com.carsome.bookingservice.repository;

import com.carsome.bookingservice.entity.BookingSlot;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingSlotRepository extends JpaRepository<BookingSlot, Long> {

  BookingSlot findOneByInspectionDate(ZonedDateTime inspectionDate);

  List<BookingSlot> findByInspectionDateBetween(ZonedDateTime start, ZonedDateTime end);
}
