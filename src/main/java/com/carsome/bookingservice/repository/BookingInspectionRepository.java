package com.carsome.bookingservice.repository;

import com.carsome.bookingservice.entity.BookingInspection;
import java.time.ZonedDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingInspectionRepository extends JpaRepository<BookingInspection, Long> {

  BookingInspection findFirstByCustomerEmailOrderByCreatedDateDesc(String email);

  Integer countByInspectionDate(ZonedDateTime inspectionDate);
}
