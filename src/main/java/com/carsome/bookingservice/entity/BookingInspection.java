package com.carsome.bookingservice.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "booking_inspections")
public class BookingInspection implements Serializable {

  private static final long serialVersionUID = -4354914841279507123L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private ZonedDateTime inspectionDate;
  private String customerName;
  private String customerEmail;
  private String customerPhoneNo;
  private ZonedDateTime createdDate;
}
