package com.carsome.bookingservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingSchedule implements Serializable {

  private static final long serialVersionUID = -6372581908976954200L;

  Date date;
  List<String> availableTime;
}
