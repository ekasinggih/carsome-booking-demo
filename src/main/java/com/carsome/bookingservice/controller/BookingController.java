package com.carsome.bookingservice.controller;

import com.carsome.bookingservice.dto.BookingInspectionDto;
import com.carsome.bookingservice.dto.BookingScheduleDto;
import com.carsome.bookingservice.service.IBookingInspectionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("booking")
@CrossOrigin(origins = "*")
public class BookingController {

  @Autowired IBookingInspectionService bookingService;

  @PostMapping(value = "/{id}")
  @ResponseBody
  public ResponseEntity<BookingInspectionDto> getById(@PathVariable Long id) {
    return new ResponseEntity<>(bookingService.getById(id), HttpStatus.OK);
  }

  @GetMapping(value = "/schedule")
  @ResponseBody
  public ResponseEntity<List<BookingScheduleDto>> getSchedule() throws JsonProcessingException {
    return new ResponseEntity<>(bookingService.getAvailableSchedule(), HttpStatus.OK);
  }

  @PostMapping
  @ResponseBody
  public ResponseEntity<BookingInspectionDto> create(@RequestBody BookingInspectionDto request) {
    return new ResponseEntity<>(bookingService.bookInspection(request), HttpStatus.CREATED);
  }
}
