package com.carsome.bookingservice.controller;

import com.carsome.bookingservice.dto.ErrorResponse;
import com.carsome.bookingservice.service.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CarsomeControllerAdvise {
  private static final Logger LOGGER = LoggerFactory.getLogger(CarsomeControllerAdvise.class);

  @ExceptionHandler(value = ValidationException.class)
  public ResponseEntity<ErrorResponse> errorHandler(ValidationException e) {
    LOGGER.error(e.getMessage(), e);
    return new ResponseEntity<>(
        new ErrorResponse(e.getMessage(), e.getErrorCode()), HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
