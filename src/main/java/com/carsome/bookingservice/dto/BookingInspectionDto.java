package com.carsome.bookingservice.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingInspectionDto implements Serializable {

  private static final long serialVersionUID = -8513315142064261622L;

  private Long id;
  private String inspectionDate;
  private String customerName;
  private String customerEmail;
  private String customerPhoneNo;
}
