package com.carsome.bookingservice.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse implements Serializable {

  private static final long serialVersionUID = -5177139901376805530L;

  private String errorCode;
  private String message;
  private long timestamp;

  public ErrorResponse(String message, String errorCode) {
    super();
    this.message = message;
    this.errorCode = errorCode;
  }
}
